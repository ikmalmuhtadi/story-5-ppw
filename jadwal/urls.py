from django.urls import path
from . import views
from .views import join, delete, detail

app_name = 'jadwal'


urlpatterns = [
    path('', join, name='Schedule'),
    path('<int:pk>', delete, name = 'delete'),
    path('detail/<int:pk>', detail, name='detail'),
]