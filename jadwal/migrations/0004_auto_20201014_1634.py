# Generated by Django 3.1.2 on 2020-10-14 16:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jadwal', '0003_auto_20201013_1755'),
    ]

    operations = [
        migrations.AlterField(
            model_name='schedule',
            name='sks',
            field=models.CharField(choices=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'), ('6', '6')], max_length=1),
        ),
    ]
