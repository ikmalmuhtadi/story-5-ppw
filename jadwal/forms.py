from django import forms

class SchedForm(forms.Form):
        matkul = forms.CharField(widget=forms.TextInput(attrs={
            'class' : 'form-control',
            'placeholder' : 'Mata Kuliah',
            'type' : 'text',
            'required' : True,
        }))
        dosen = forms.CharField(widget=forms.TextInput(attrs={
            'class' : 'form-control',
            'placeholder' : 'Nama Dosen',
            'type' : 'text',
            'required' : True,
        }))
        sks = forms.IntegerField(widget=forms.TextInput(attrs={
            'class' : 'form-control',
            'placeholder' : 'Jumlah SKS',
            'type' : 'text',
            'required' : True,
        }))
        deskripsi = forms.CharField(widget=forms.TextInput(attrs={
            'class' : 'form-control',
            'placeholder' : 'Deskripsi Mata Kuliah',
            'type' : 'text',
            'required' : True,
        }))
        semester = forms.CharField(widget=forms.TextInput(attrs={
            'class' : 'form-control',
            'placeholder' : 'Semester',
            'type' : 'text',
            'required' : True,
        }))
        kelas = forms.CharField(widget=forms.TextInput(attrs={
            'class' : 'form-control',
            'placeholder' : 'Kelasmu di ruang berapa',
            'type' : 'text',
            'required' : True,
        }))

